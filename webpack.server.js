const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: './server/index.tsx',

    target: 'node',

    externals: [nodeExternals()],

    output: {
        path: path.resolve('server-build'),
        filename: 'index.js'
    },

    resolve: {
        modules: [
            path.join(__dirname, 'src'),
            'node_modules'
        ],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
        alias: {
			'@src': path.resolve(__dirname, 'src')
		},
    },

    module: {
        rules: [
            
            {
                test: /\.(tsx|ts)?$/,
                use: [
                    {loader: 'babel-loader'}, 
                    {loader: 'ts-loader',
                    options: {
                            compilerOptions: {
                                'noEmit': false,
                            }
                        },
                    }
                ],
            },
            {
                test: /\.(css|scss)$/,
                use: [
                    {loader: 'css-loader'}, 
                    {loader: 'sass-loader'},
                ]
            },
            {
                test: /\.(ico|png|jpg|jpeg|gif|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader',
                options: {
                    publicPath: './static',
                    outputPath: '../public/static',
                    name: 'images/[name].[hash].[ext]',
                    limit: 0,
                    esModule: false,
                }
            },
            {
                test: /\.(woff|woff2|ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader',
                options: {
                    publicPath: './static',
                    outputPath: '../public/static',
                    name: 'fonts/[name].[hash].[ext]',
                    limit: 0,
                    esModule: false,
                }
            },

        ]
    }
};