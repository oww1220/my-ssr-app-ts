# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## release
release 명령어
`npm run release`

## Prettier 
create react app 기준<br />
프리티어는 코드 정리 도구입니다.<br />
설치<br />
`npm install --save-dev --save-exact prettier`<br />
eslint-config-prettier 도구를 통해서 prettier와 겹치는 rule은 무시시켜줍니다.<br />
예를 들면 semi, tab 같은 부분이 있습니다.<br />
`npm install --save-dev eslint-config-prettier`<br />
package.json 설정파일에<br />
"eslintConfig": {<br />
    "extends": [<br />
      "react-app",<br />
      "react-app/jest",<br />
      `"prettier"`<br />
    ]<br />
},<br />
prettier 추가해줌

## Lint-Staged
린트(lint) 또는 린터(linter)는 소스 코드를 분석하여 프로그램 오류, 버그, 스타일 오류, 의심스러운 구조체에 표시(flag)를 달아놓기 위한 도구들을 가리킨다.<br/>
lint를 staged에 있는 파일을 체크함으로서, lint:fix를 통과하지 않는 파일은 커밋 하지 않게 도와줍니다.<br />
설치<br />
`npm i --save-dev lint-staged@next`<br/>
`yarn lint-staged` 로 수동실행

## Husky
githooks 를 npm 을 통해서 관리할 수 있게 도와주는 라이브러리 입니다.<br />
최신버전은 오류가 있어서 4버전때로 설치<br />
`npm i --save-dev husky@4`

### staging에 있는 파일을 npm 으로 다루기 위해서 → lint-staged
### git hooks를 npm으로 다루기 위해서 → husky 를 이용한다. 
### 이 두가지를 조합해서 commit 하기 전에 파일들을 lint 룰이 적용된 파일만 커밋할 수 있게 도와줍니다.

## 도커 명령어
<빌드 명령어-이미지 생성><br />
`docker build -t my-ssr-app-ts:0.1.1 .`<br />

<런 명령어-이미지기반 컨테이너 생성><br />
`docker run -p 83:3006 --name my-ssr-app-ts my-ssr-app-ts:0.1.1`<br />

*도커 내부 컨테이너 안에서 <호스트 localhost>는 접근이 안됨,<br/>
도커를 깔면 호스트 파일에 host.docker.internal 세팅이 자동으로 됨 <br/>
<br/>
Docker Compose란<br/>
복수 개의 컨테이너를 실행시키는 도커 애플리케이션이 정의를 하기 위한 툴입니다.<br/>
Compose를 사용하면 YAML 파일을 사용하여 애플리케이션의 서비스를 구성할 수 있습니다.<br/>
docker-compose 로 해결은 가능할듯하나 테스트는 안해봄.. 아직 app서버는 도커이미자화 안시킴 <br/>
docker-compose.yml - 실행내용 : (nginx서버는 reverse Proxy 서버로 다른포트의 express서버를 연결해주고 express서버를 감추어준다!)<br/>
실행문!<br/>

`docker-compose up -d`

## env-cmd
package.json 내부에 scripts에서 env-cmd -f .env.dev이런식으로 env 파일을 강제로 실행시키게 해준다!<br/>
`npm i env-cmd`

## npm-run-all
이 패키지는 npm의 여러 스크립트를 동시에 실행시켜줄수 있는 패키지로, 이번 포스팅처럼 리액트 서버와 웹서버를 동시에 실행하여야 할때 매우 간편합니다.<br/>
`npm-run-all --parallel` 로 실행시 병렬처리<br/>
`npm install -D npm-run-all`

## redux-saga-test-plan
redux-saga-test-plan은 사가(Saga) 제너레이터 함수를 테스트할 때, 실제 구현 로직과 테스트 코드가 갖는 커플링, 그리고 매뉴얼 한 테스트에 대한 문제를 해결해준다. <br />
테스트에 선언적이고, 체이닝(chainable) API를 제공해서 실제 구현체인 사가에서 원하는 이펙트만을 테스트할 수 있도록 도와준다. <br />
이외 어떤 이펙트들을 나타내는지, 이펙트들의 순서는 어떻게 되는지 신경쓰지 않고 걱정하지 않도록 한다. <br />
redux-saga의 런타임을 함께 사용하므로, 통합 테스트를 할 수도 있고, redux-saga-test-plan에 내장된 이펙트 목킹(mocking)을 활용해 유닛 테스트도 작성할 수 있다.<br />
`npm i -D redux-saga-test-plan`

## redux-mock-store
redux-mock-store는 상태를 업데이트 하지 않고 디스패치에 전달된 작업만 기록하기 때문에, store.dispatch() 구문 실행 후 store.getState()로 변경된 상태를 확인하는 것이 불가능합니다.<br />
해당 라이브러리가 리듀서 관련 로직이 아닌 액션 관련 로직을 테스트하도록 설계되었기 때문에 이는 어쩔 수 없는 부분이 아닌가 생각합니다.<br />
`npm i -D redux-mock-store`<br />
`npm i -D @types/redux-mock-store`

## 타입스크립트 패키지 적용시
https://www.typescriptlang.org/dt/search?search=<br/>
에서 패키지 찾아서 설치

## 호스트 정보(호스트 파일) 
서버 컴퓨터 ip 적용(예시부분) <br />
172.28.1.73(was서버 ip) lapi.react-ssr.com<br />
`react-test-ssr`<br />
`172.28.1.73 lapi.react-ssr.com`<br />
`127.0.0.1 lwww.react-ssr.com`


## 연결 url
http://lwww.react-ssr.com:83  <br />
http://lwww.react-ssr.com:3000  <br />
http://lwww.react-ssr.com:3006  <br />

