import { History } from 'history';

/*반복 인터페이스 정의*/
export type paramHistory = History;

export interface IUsersData {
    _id?: string;
    user_id: string;
    user_name: string;
    password?: string;
    gender: string;
    age: number;
}

export interface IlogInData {
    user_id: string;
    password: string;
}

export interface IAuthData {
    couponCnt: number;
    estimateSaveCnt: number;
}

export interface IClassNames {
    [className: string]: string;
}

export interface IErrors {
    /* The validation error messages for each field (key is the field name */
    [key: string]: string;
}
