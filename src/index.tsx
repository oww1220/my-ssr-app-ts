import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Root from 'store/Root';
import reportWebVitals from 'reportWebVitals';
import { HelmetProvider } from 'react-helmet-async';


/*redux*/
import rootSaga from 'store/saga/RootSaga';
import ConfigureStore from 'store/ConfigureStore';
import { State } from 'store/reducer/RootReducer';

/*apollo*/
import ConfigureClient from 'store/ConfigureClient';

/*기존 모듈에 프라퍼티 선언 추가*/
declare global {
	interface Window {
		__APP_INITIAL_STATE__?: State;
	}
	interface NodeModule {
		hot: any;
	}
}

/*redux*/
const initialState = window.__APP_INITIAL_STATE__;
delete window.__APP_INITIAL_STATE__;
const store = ConfigureStore(initialState);
store.runSaga(rootSaga);

/*apollo*/
const client = ConfigureClient();

const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate;

renderMethod(
	<React.StrictMode>
		<HelmetProvider>
			<Router>
				<Root store={store} client={client}/>
			</Router>
		</HelmetProvider>
	</React.StrictMode>,
	document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
