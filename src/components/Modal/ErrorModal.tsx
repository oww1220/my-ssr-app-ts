import React, { useState, useCallback, useEffect } from 'react';
import Modal from 'components/Modal/Modal';

interface IErrorModal {
	errorData?:any;
}

//let currentData:any = null;

export const ErrorModal = ({errorData}:IErrorModal)=> {
	console.log('call:', ErrorModal.name, '!!');

	//console.log('errorModal',errorData);
	//console.log('비교', currentData===errorData);
	//currentData = errorData;

	return (
		<Modal 
			content={
				<div>{errorData.data.message}</div>
			}
			buttonVisible={false}
			dependence={errorData}
			cssStyle={{'width':'400px', 'background':'#fff'}}
		/>
		
	);
};

export default ErrorModal;
