import React, { useState, useCallback, useEffect } from 'react';
import Modal from 'components/Modal/Modal';
import useTestList from 'store/hooks/UseTestList';

export const NormalModal = () => {
    console.log('call:', NormalModal.name, '!!');

    const { testLists, fetchTestList } = useTestList();

    useEffect(() => {
        console.log('!!!!effect!!!!');
        fetchTestList();
    }, []);

    return (
        <Modal
            content={
                <div>
                    <ul>
                        {testLists.map(({ age, _id }, idx: number) => (
                            <li key={_id}>{age}</li>
                        ))}
                    </ul>
                </div>
            }
            cssStyle={{ width: '700px', background: '#fff' }}
            classFlag={'normalModal'}
        />
    );
};

export default NormalModal;
