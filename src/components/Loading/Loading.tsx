import React from 'react';
import roader from 'assets/images/loader.svg';

function Loading() {
	return (
		<div id="pageloading" className="pageloading_wrap" style={{"display":"block"}}>
			<div className="pageloading">
				<img src={roader} alt="loading"/>
			</div>
		</div>
	);
}

export default Loading;
