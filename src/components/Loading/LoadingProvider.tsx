import React from 'react';
import useLoad from 'store/hooks/UseLoad';
import Loading from 'components/Loading/Loading';

interface ILoadingProvider {
	children: JSX.Element;
}
const LoadingProvider = ({ children }:ILoadingProvider)=> {
	console.log('call:', LoadingProvider.name, '!!');

	const { loadState } = useLoad();

	return (
		<>
			{children}
			{loadState && <Loading />}
		</>
	);
};

export default LoadingProvider;
