import { useMemo } from 'react';
import { createPortal } from 'react-dom';

interface IPortal {
	children: JSX.Element;
	elementId: string;
}

const Portal = ({ children, elementId }:IPortal)=> {
	const rootElement = useMemo(() => document.getElementById(elementId), [
		elementId,
	]);
	return rootElement && createPortal(children, rootElement);
};

export default Portal
