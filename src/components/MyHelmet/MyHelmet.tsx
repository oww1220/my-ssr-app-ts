import React from 'react';
import { Helmet } from 'react-helmet-async';


interface MyHelmetProps {
	title: string;
	description: string;
}

function MyHelmet({title, description}: MyHelmetProps) {
	//console.log(title);
	return (
		<Helmet>
			<meta charSet="utf-8" />
			{description && <meta name="description" content={description} />}
			<title>{title}</title>
		</Helmet>
	);
}

export default MyHelmet;
