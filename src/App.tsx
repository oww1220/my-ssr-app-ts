import React from 'react';
import Layout from 'container/Layout/Layout';
import 'assets/scss/Root.scss';

function App() {
  return (
    <Layout />
  );
}

export default App;
