import React from 'react';
import { Link } from 'react-router-dom';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import Config from 'lib/Config';
import logo from 'assets/images/logo.svg';
import logo3 from 'assets/images/logo192.png';
import NormalModal from 'components/Modal/NormalModal';

const Home = ()=> {
	return (
		<>
		<MyHelmet title={'Home'} description={'Home'}/>

		<div className="App-header">
			
			<h1>{Config.APP_ENV}:Home ?? <span className="app-version">v.{Config.APP_VERSION}</span></h1>

			<div className="home-nav">
				<Link to="/TestList">
					TestList 로 이동(csr)
				</Link>
				<a href="/TestList">TestList 로 이동(ssr)</a>

				<Link to="/TestNormal">
					TestNormal 로 이동(csr)
				</Link>
				<a href="/TestNormal">TestNormal 로 이동(ssr)</a>

				<Link to="/TestApollo">
					TestApollo 로 이동(csr)
				</Link>
				<a href="/TestApollo">TestApollo 로 이동(ssr)</a>

				<Link to="/TestSaga">
					TestSaga 로 이동(csr)
				</Link>
				<a href="/TestSaga">TestSaga 로 이동(ssr)</a>

				<Link to="/AuthPage">
					AuthPage 로 이동(csr)
				</Link>
				<a href="/AuthPage">AuthPage 로 이동(ssr)</a>
			</div>

			<img src={logo} className="App-logo" alt="logo" />
			<img src={logo3} className="App-logo3" alt="logo3" />

			<NormalModal />

		</div>
		</>
	);
}

export default Home;
