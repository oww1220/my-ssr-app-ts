import React, { useCallback } from 'react';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import { useForm } from 'react-hook-form';
import useLogIn from 'store/hooks/UseLogIn';

const LogIn = ()=> {
	const { linkLogIn } = useLogIn();
	const { register, handleSubmit, watch, errors } = useForm();

	const onSubmit = useCallback(
		(data) => {
			linkLogIn(data);
			//console.log(data);
	}, []);


	return (
		<>
		<MyHelmet title={'LogIn'} description={'LogIn'}/>
		<div className="LogIn">
			<h1>LogIn</h1>
			<form>
				<input type={"text"} name="user_id" ref={register({ required: true })} placeholder={'user_id'}/>
				{errors.user_id && "user_id is required"}
				<input type={"text"} name="password" ref={register({ required: true })} placeholder={'password'}/>
				{errors.password && "password is required"}
			</form>

			<button type="button" className="btn" onClick={handleSubmit(onSubmit)}>
				로그인 
            </button>
		</div>
		</>
		
	)
};

export default LogIn;
