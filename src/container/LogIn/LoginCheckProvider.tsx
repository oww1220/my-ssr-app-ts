import React, { useEffect } from 'react';
import useReactRouter from 'use-react-router';
import useToken from 'store/hooks/UseToken';
import useHistory from 'store/hooks/UseHistory';
import * as Commons from 'lib/Commons';
import Config from 'lib/Config';

interface ILoginCheckProvider {
    children: JSX.Element;
}
const LoginCheckProvider = ({ children }: ILoginCheckProvider) => {
    console.log('call:', LoginCheckProvider.name, '!!');

    const { location, history } = useReactRouter();
    const { setToken } = useToken();
    const { historyChange } = useHistory();

    //url 변경시마다 토큰체크해서 토큰 setter (토큰유무에 따라 로그인 여부결정함)~~
    useEffect(() => {
        const getToken = Commons.getBearerToken();
        setToken(getToken);

        //gitlab pages redirect use
        if (Config.APP_NAME && Commons.regExp(location.pathname) === Commons.regExp(Config.APP_NAME)) {
            console.log('APP_NAME', Config.APP_NAME, Commons.regExp(location.pathname));
            history.push('/');
        }
        //로케이션 변경시마다 히스토리 객체 리덕스 스토어에 저장..
        //redux-saga 내부에서 history 객체를 참조하기 위해서....
        else {
            historyChange(history);
        }
    }, [location.pathname]);

    return children;
};

export default LoginCheckProvider;
