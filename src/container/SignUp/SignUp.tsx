import React, { useCallback } from 'react';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import { useForm } from 'react-hook-form';
import useReactRouter from 'use-react-router';
import useLogIn from 'store/hooks/UseLogIn';

const SignUp = () => {
    const { linkSignUp } = useLogIn();
    const { history } = useReactRouter();
    const { register, handleSubmit, watch, errors } = useForm();
    const onSubmit = useCallback((data) => {
        linkSignUp(data);
    }, []);

    return (
        <>
            <MyHelmet title={'SignUp'} description={'SignUp'} />
            <div className="SignUp">
                <h1>SignUp</h1>

                <form>
                    <input type={'text'} name="user_id" ref={register({ required: true })} placeholder={'user_id'} />
                    {errors.user_id && 'user_id is required'}

                    <input
                        type={'text'}
                        name="user_name"
                        ref={register({ required: true })}
                        placeholder={'user_name'}
                    />
                    {errors.user_name && 'user_name is required'}

                    <input type={'text'} name="age" ref={register({ required: true })} placeholder={'age'} />
                    {errors.age && 'age is required'}

                    <input type={'text'} name="gender" ref={register({ required: true })} placeholder={'gender'} />
                    {errors.gender && 'gender is required'}

                    <input type={'text'} name="password" ref={register({ required: true })} placeholder={'password'} />
                    {errors.password && 'password is required'}
                </form>

                <button type="button" className="btn" onClick={handleSubmit(onSubmit)}>
                    전송
                </button>
            </div>
        </>
    );
};

export default SignUp;
