import React from 'react';
import Routes from 'router/Routes';
import Header from 'container/Layout/Header';
import ErrorProvider from 'components/Error/ErrorProvider';
import LoadingProvider from 'components/Loading/LoadingProvider';
import LoginCheckProvider from 'container/LogIn/LoginCheckProvider';

function Layout() {
	return (
		<LoadingProvider>
			<ErrorProvider>
				<LoginCheckProvider>
					<div className="App">
						<Header />
						<Routes />
					</div>
				</LoginCheckProvider>
			</ErrorProvider>
		</LoadingProvider>
	)
}

export default Layout;
