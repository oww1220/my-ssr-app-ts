import React, { useEffect, useCallback } from 'react';
import { Link } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import useLogIn from 'store/hooks/UseLogIn';

function Header() {
    const { history, location } = useReactRouter();
    const { logInState, logInStateData, linkLogOut } = useLogIn();

    const historyBack = useCallback(() => {
        history.goBack();
    }, []);

    const LinkLogOut = useCallback(() => {
        linkLogOut();
    }, []);

    /*
	useEffect(() => {
		console.log(history, location.pathname);
	});*/

    return (
        <>
            <div className="Header">
                <div className="left">
                    {location.pathname === '/' ? (
                        ''
                    ) : (
                        <button type="button" onClick={historyBack}>
                            이전으로
                        </button>
                    )}
                </div>
                <div className="right">
                    {logInState ? (
                        <div>
                            <span>{logInStateData && logInStateData.user_id} 님</span>
                            <Link to="/UpdateUser">회원정보 수정</Link>
                            <Link to="/DeleteUser">회원탈퇴</Link>
                            <button type="button" onClick={LinkLogOut}>
                                로그아웃 하기
                            </button>
                        </div>
                    ) : (
                        <>
                            <Link to="/SignUp">SignUp 로</Link>
                            <Link to="/LogIn">LogIn 로</Link>
                        </>
                    )}
                </div>
            </div>
        </>
    );
}

export default Header;
