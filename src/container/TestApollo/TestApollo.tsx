import React, {useEffect, useCallback} from 'react';
import { useQuery } from 'react-apollo';
import * as Api from 'lib/Api';

import Loading from 'components/Loading/Loading';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import * as Interface from 'interfaces/Interfaces';


function TestApollo() {
  const { loading, error, data } = useQuery(Api.GET_ALLUSER);

  console.log(loading, error, data);

  if(loading) return <Loading />;

  if(error) {
    console.log(error.message)
    return (
      <div className={"posts-error-message"}>error occured!</div>
    )
  };

  return (
    <>
      <MyHelmet title={'TestApollo'} description={'TestApollo'}/>
      <div className="App-header">
        <h1>Test!!!!</h1>
            
        {data &&
          <ul>
          {data.allUser.map(({ gender, _id }:Interface.IUsersData, idx:number) => (
            <li key={_id}>아폴로{gender}</li>
          ))}
          </ul>
        }
        
      </div>
    </>
  );
}

export default TestApollo;
