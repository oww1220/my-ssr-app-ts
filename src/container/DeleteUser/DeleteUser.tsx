import React, { useCallback } from 'react';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import { useForm } from 'react-hook-form';
import useLogIn from 'store/hooks/UseLogIn';

const DeleteUser = () => {
    const { logInStateData, deleteUser } = useLogIn();

    const onSubmit = useCallback(() => {
        if (logInStateData) {
            alert('정말 탈퇴하시겠습니까?');
            deleteUser(logInStateData);
        }
    }, [logInStateData]); //의존성 배열에 빈 값을 넣으면 새로고침시 로그인 체크 할 경우 함수 클로져환경이 안바뀜(로그인 체크 로직이 effect안에 들어있어서..)..그래서 logInStateData 이 null로 유지됨

    return (
        <>
            <MyHelmet title={'DeleteUser'} description={'DeleteUser'} />
            <div className="LogIn">
                <h1>회원탈퇴</h1>
                {logInStateData && (
                    <button type="button" className="btn" onClick={onSubmit}>
                        회원탈퇴
                    </button>
                )}
            </div>
        </>
    );
};

export default DeleteUser;
