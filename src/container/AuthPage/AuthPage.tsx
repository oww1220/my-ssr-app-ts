import React, { useCallback, useEffect, useState } from 'react';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import { ParsedQuery } from 'query-string';
import { IComponent } from 'router/RouteList';
import useError from 'store/hooks/UseError';
import useLogIn from 'store/hooks/UseLogIn';
import useTestList from 'store/hooks/UseTestList';
import * as Api from 'lib/Api';
import * as Interface from 'interfaces/Interfaces';

const AuthPage:IComponent = ()=> {
	const { failureLoad } = useError();
	const { logInStateData, logInState } = useLogIn();
	const { authObj, fetchAuthList } = useTestList();
	//const [ userData, setUserData ] = useState({couponCnt:0, estimateSaveCnt:0});

	/*ssr용 메서드---추후 saga로*/
	AuthPage.getInitialData = async (params:ParsedQuery) => {
		console.log('params', params);

		//인증이 필요한 페이지에서는 로그인상태 확인후 함수호출!!
		if(logInState) fetchAuthList();
	};
	/*useEffect 는  prop과 state를 잡아둠,  만약 “최신의” 상태 등을 원한다면, useRef 사용*/
	//모든 랜더링은 고유의 이펙트, 이벤트 핸들러, Prop과 State(즉 모든것) 를 가진다!!
	//리액트는 브라우저가 페인트 하고 난 뒤에야 이펙트를 실행합니다. 이렇게 하여 대부분의 이펙트가 스크린 업데이트를 가로막지 않기 때문에 앱을 빠르게 만들어줍니다. 
	//마찬가지로 이펙트의 클린업도 미뤄집니다. 이전 이펙트는 새 prop과 함께 리랜더링 되고 난 뒤에 클린업됩니다.
	useEffect(() => {
		let test = false;
		/*
		console.log('logInStateData',logInStateData);
		if(logInStateData) {
			
			Api.getAuthList(logInStateData.user_id).then(({data})=>{
				console.log('data!!!',data.data.authUser);
				setUserData(data.data.authUser);
			})
			.catch((error)=>{
				failureLoad(error.response);
				//alert(error.response.data.message);
				//console.log(error);
			});
			
		}*/
		//clean up 함수...컴퍼넌트가 언마운트되거나, 새로 업데이트 되기 전에 실행...즉 prop과 state는 이전 잡아둔 상태를 따름
		return () => {
			console.log('return',test);
			test = true;
		}
	},[logInStateData]);//의존성 배열

	//사가로 불러옴
	useEffect(() => {
		if(logInStateData) fetchAuthList();
	},[logInStateData]);

	return (
		<>
		<MyHelmet title={'AuthPage'} description={'AuthPage'}/>
		<div className="AuthPage">
			로그인후 진입가능<br/>
			couponCnt: {authObj && authObj.couponCnt},<br/>
			estimateSaveCnt: {authObj && authObj.estimateSaveCnt},

		</div>
		
		</>
		
	)
}

export default AuthPage;
