import React, { useCallback } from 'react';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import { useForm } from 'react-hook-form';
import useLogIn from 'store/hooks/UseLogIn';

const UpdateUser = () => {
    const { logInStateData, updateUser } = useLogIn();
    const { register, handleSubmit, watch, errors } = useForm();

    const onSubmit = useCallback(
        (data) => {
            //console.log(11111111, logInStateData);
            if (logInStateData) {
                const userData = { _id: logInStateData._id, ...data };
                updateUser(userData);
            }
        },
        [logInStateData],
    ); //의존성 배열에 빈 값을 넣으면 새로고침시 로그인 체크 할 경우 함수 클로져환경이 안바뀜(로그인 체크 로직이 effect안에 들어있어서..)..그래서 logInStateData 이 null로 유지됨

    return (
        <>
            <MyHelmet title={'UpdateUser'} description={'UpdateUser'} />
            <div className="LogIn">
                <h1>회원정보수정</h1>
                {logInStateData && (
                    <>
                        <form>
                            <span>아이디</span>
                            <input
                                type={'text'}
                                name="user_id"
                                ref={register({ required: true })}
                                placeholder={'user_id'}
                                defaultValue={logInStateData.user_id}
                            />
                            {errors.user_id && 'user_id is required'}
                            <span>이름</span>
                            <input
                                type={'text'}
                                name="user_name"
                                ref={register({ required: true })}
                                placeholder={'user_name'}
                                defaultValue={logInStateData.user_name}
                            />
                            {errors.user_name && 'user_name is required'}
                            <span>성별</span>
                            <input
                                type={'text'}
                                name="gender"
                                ref={register({ required: true })}
                                placeholder={'gender'}
                                defaultValue={logInStateData.gender}
                            />
                            {errors.gender && 'gender is required'}
                            <span>나이</span>
                            <input
                                type={'text'}
                                name="age"
                                ref={register({ required: true })}
                                placeholder={'age'}
                                defaultValue={logInStateData.age}
                            />
                            {errors.age && 'age is required'}
                        </form>
                        <button type="button" className="btn" onClick={handleSubmit(onSubmit)}>
                            회원정보수정
                        </button>
                    </>
                )}
            </div>
        </>
    );
};

export default UpdateUser;
