import React, { useEffect, useCallback, useState } from 'react';
import { useQuery } from 'react-apollo';
import * as Api from 'lib/Api';
import queryString from 'query-string';
import useReactRouter from 'use-react-router';
import useTestList from 'store/hooks/UseTestList';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import logo3 from 'assets/images/logo192.png';
import { ParsedQuery } from 'query-string';
import { IComponent } from 'router/RouteList';
import * as Interface from 'interfaces/Interfaces';

/*test 컨퍼넌트*/
const TestCountInner = () => {
    //console.log('call:', TestCountInner.name, '!!');
    return <span>현재</span>;
};

/*카우터 테스터 */
export const Counter = () => {
    console.log('call:', Counter.name, '!!');
    const [count, setCount] = useState(0);

    useEffect(() => {
        const id = setInterval(() => {
            setCount((count) => count + 1);
        }, 1000);
        return () => {
            console.log('clear');
            return clearInterval(id);
        };
    }, []);
    return (
        <h1>
            <TestCountInner />
            {count}
        </h1>
    );
};

const TestList: IComponent = () => {
    console.log('call:', TestList.name, '!!');

    const { testLists, fetchTestList } = useTestList();
    const { history, location, match } = useReactRouter();
    const { loading, error, data } = useQuery(Api.GET_ALLUSER);

    //console.log(loading, error, data);

    /*ssr용 메서드*/
    TestList.getInitialData = (params: ParsedQuery) => {
        console.log('params', params);
        /*action 함수를 리턴*/
        fetchTestList();
    };

    useEffect(() => {
        console.log('!!!!effect!!!!');
        const query = queryString.parse(location.search);
        console.log(location, query);
        fetchTestList();
    }, []);

    return (
        <>
            <MyHelmet title={'TestList'} description={'TestList'} />
            <div className="App-header">
                <Counter />
                <h1>Test!!!!</h1>
                <img src={logo3} className="App-logo3" alt="logo3" />
                <ul>
                    {testLists.map(({ age, _id }, idx: number) => (
                        <li key={_id}>{age}</li>
                    ))}
                </ul>

                {data && (
                    <ul>
                        {data.allUser.map(({ gender, _id }: Interface.IUsersData, idx: number) => (
                            <li key={_id}>아폴로{gender}</li>
                        ))}
                    </ul>
                )}
            </div>
        </>
    );
};

export default TestList;
