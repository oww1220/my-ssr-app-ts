import React, {useEffect, useCallback} from 'react';
import useTestList from 'store/hooks/UseTestList';
import MyHelmet from 'components/MyHelmet/MyHelmet';
import logo3 from 'assets/images/logo192.png';
import { ParsedQuery } from 'query-string';
import { IComponent } from 'router/RouteList';

const TestSaga:IComponent = ()=>{
  console.log('call:', TestSaga.name, '!!');

  const { testLists, fetchTestList } = useTestList();

  /*ssr용 메서드*/
  TestSaga.getInitialData = useCallback((params: ParsedQuery) => {
    console.log('params', params);
    /*action 함수를 리턴*/
    fetchTestList();
  },[]);

  useEffect(() => {
    fetchTestList();
  },[]);

  return (
    <>
      <MyHelmet title={'TestSaga'} description={'TestSaga'}/>
      <div className="App-header">
        <h1>Test!!!!</h1>
        <img src={logo3} className="App-logo3" alt="logo3" />
        <ul>
          {testLists.map(({ age, _id }, idx:number) => (
            <li key={_id}>{age}</li>
          ))}
        </ul>
        
      </div>
    </>
  )
}


export default TestSaga;
