import React, { useCallback, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import useError from 'store/hooks/UseError';
import {IRouteList} from 'router/RouteList';
import * as Commons from 'lib/Commons';


function AuthRoute({path, component, exact}: IRouteList) {
	const { history } = useReactRouter();
	const { failureLoad } = useError();

	const alertRedirect = useCallback(
		() => {
			//if (typeof window !== 'undefined') { alert('로그인 필요'); }
			const error = {data:Error('로그인 필요!')};
			failureLoad(error);
			history.push('/LogIn');
			//return <Redirect to="/LogIn" />
	},[]);
	
	useEffect(()=> {
		console.log('authpage-chck');
		const getToken = Commons.getBearerToken();
		if(!getToken) alertRedirect();
	},[]);

	return (
		<Route 
			path={path}
			component={component}
			exact={exact}
		/>
	);
}

export default AuthRoute;
