import Home from 'container/Home/Home';
import TestList from 'container/TestList/TestList';
import TestNormal from 'container/TestNormal/TestNormal';
import TestApollo from 'container/TestApollo/TestApollo';
import TestSaga from 'container/TestSaga/TestSaga';
import SignUp from 'container/SignUp/SignUp';
import LogIn from 'container/LogIn/LogIn';
import AuthPage from 'container/AuthPage/AuthPage';
import UpdateUser from 'container/UpdateUser/UpdateUser';
import DeleteUser from 'container/DeleteUser/DeleteUser';

export interface IComponent {
    (): JSX.Element;
    getInitialData?: any;
}

export interface IRouteList {
    path: string;
    component: IComponent;
    exact: boolean;
    auth?: boolean;
}

const RouteList: IRouteList[] = [
    {
        path: '/',
        component: Home,
        exact: true,
    },
    {
        path: '/TestNormal',
        component: TestNormal,
        exact: false,
    },
    {
        path: '/TestSaga',
        component: TestSaga,
        exact: false,
    },
    {
        path: '/TestApollo',
        component: TestApollo,
        exact: false,
    },
    {
        path: '/TestList',
        component: TestList,
        exact: false,
    },
    {
        path: '/SignUp',
        component: SignUp,
        exact: false,
    },
    {
        path: '/LogIn',
        component: LogIn,
        exact: false,
    },
    {
        path: '/AuthPage',
        component: AuthPage,
        exact: false,
        auth: true,
    },
    {
        path: '/UpdateUser',
        component: UpdateUser,
        exact: false,
        auth: true,
    },
    {
        path: '/DeleteUser',
        component: DeleteUser,
        exact: false,
        auth: true,
    },
];

export default RouteList;
