import React from 'react';
import { Route, Switch } from 'react-router-dom';

import RouteList, {IRouteList} from 'router/RouteList';
import AuthRoute from 'router/AuthRoute';
import NotFound from 'container/NotFound/NotFound';

function Routes() {
	return (
		<>
		<Switch>
			{
			RouteList.map((route:IRouteList, idx) => {
				if(!route.auth) {
					return (<Route key={idx} {...route}/>)
				}
				else {
					return (<AuthRoute key={idx} {...route} />)

				}
			})
			}
			<Route component={NotFound} />
		</Switch>
		</>
	)
}

export default Routes;
