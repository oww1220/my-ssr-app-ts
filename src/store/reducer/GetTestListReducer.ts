import * as Interface from 'interfaces/Interfaces';

/*액션상수*/
export const FETCH_TESTLIST = 'GetTestListReducer/FETCH_TESTLIST' as const;
export const TESTLIST_SUCCESS = 'GetTestListReducer/TESTLIST_SUCCESS' as const;

export const FETCH_AUTHLIST = 'GetTestListReducer/FETCH_AUTHLIST' as const;
export const AUTHLIST_SUCCESS = 'GetTestListReducer/AUTHLIST_SUCCESS' as const;
export const AUTHLIST_RESET = 'GetTestListReducer/AUTHLIST_RESET' as const;

/*액션함수*/
export const fetchTestList = () => ({
    type: FETCH_TESTLIST,
});
export const fetchTestSuccess = (testLists: Interface.IUsersData[]) => ({
    type: TESTLIST_SUCCESS,
    payload: { testLists },
});

export const fetchAuthList = () => ({
    type: FETCH_AUTHLIST,
});
export const fetchAuthSuccess = (authObj: Interface.IAuthData) => ({
    type: AUTHLIST_SUCCESS,
    payload: { authObj },
});
export const authListReset = () => ({
    type: AUTHLIST_RESET,
});

/*기본상태 및 리듀서*/
export interface IGetTestListState {
    testLists: Interface.IUsersData[];
    authObj: Interface.IAuthData | null;
}
export const initialState = {
    testLists: [],
    authObj: null,
};
type GetTestListAction =
    | ReturnType<typeof fetchTestList>
    | ReturnType<typeof fetchTestSuccess>
    | ReturnType<typeof fetchAuthList>
    | ReturnType<typeof fetchAuthSuccess>
    | ReturnType<typeof authListReset>;

export function GetTestListReducer(state: IGetTestListState = initialState, action: GetTestListAction) {
    switch (action.type) {
        case FETCH_TESTLIST:
            return {
                ...state,
            };

        case TESTLIST_SUCCESS:
            return {
                ...state,
                testLists: action.payload.testLists,
            };

        case AUTHLIST_SUCCESS:
            return {
                ...state,
                authObj: action.payload.authObj,
            };

        case AUTHLIST_RESET:
            return {
                ...state,
                authObj: null,
            };

        default:
            return state;
    }
}
