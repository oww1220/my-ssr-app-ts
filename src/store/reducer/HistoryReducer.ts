import * as Interface from 'interfaces/Interfaces';

/*액션상수*/
export const HISTORY_CHANGE = 'HistoryReducer/HISTORY_CHANGE' as const;


/*액션함수*/

export const historyChange = (history:Interface.paramHistory) => ({
    type: HISTORY_CHANGE, 
    payload: { history }, 
});

/*기본상태 및 리듀서*/
export interface IHistoryState {
    history: Interface.paramHistory | null;
}

const initialState = {
    history: null,
};

type HistoryAction =
    | ReturnType<typeof historyChange>
;

export function HistoryReducer(state:IHistoryState =initialState, action:HistoryAction) {
    switch (action.type) {

        case HISTORY_CHANGE:
        return {
            ...state,
            history: action.payload.history,
        };

        default:
        return state;

    }
}


