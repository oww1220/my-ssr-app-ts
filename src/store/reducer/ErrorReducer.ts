import * as Interface from 'interfaces/Interfaces';

/*액션상수*/
export const FAILURE_LOAD = 'ErrorReducer/FAILURE_LOAD' as const;


/*액션함수*/
export const failureLoad = (error:any) => ({
    type: FAILURE_LOAD, 
    payload: { error }, 
});


/*기본상태 및 리듀서*/
export interface IErrorState {
    errorData: any;
}

const initialState = {
    errorData: null,
};

type ErrorAction =
    | ReturnType<typeof failureLoad>
;

export function ErrorReducer(state:IErrorState =initialState, action:ErrorAction) {
    switch (action.type) {
        case FAILURE_LOAD:
        return {
            ...state,
            errorData: action.payload.error,
        };

        default:
        return state;

    }
}


