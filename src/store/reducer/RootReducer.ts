import { combineReducers } from 'redux';

import { LoadReducer, ILoadState } from 'store/reducer/LoadReducer';
import { ErrorReducer, IErrorState } from 'store/reducer/ErrorReducer';
import { HistoryReducer, IHistoryState } from 'store/reducer/HistoryReducer';
import { LogInReducer, ILogInState } from 'store/reducer/LogInReducer';
import { GetTestListReducer, IGetTestListState } from 'store/reducer/GetTestListReducer';

export type State = {
    LoadReducer: ILoadState,
    ErrorReducer: IErrorState,
    HistoryReducer: IHistoryState,
    LogInReducer: ILogInState,
    GetTestListReducer: IGetTestListState,
};

const rootReducer = combineReducers({
    LoadReducer,
    ErrorReducer,
    HistoryReducer,
    LogInReducer,
    GetTestListReducer,
});

export default rootReducer;
