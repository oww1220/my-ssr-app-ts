import * as Interface from 'interfaces/Interfaces';

/*액션상수*/
//로그인
export const LINK_LOGIN = 'LogInReducer/LINK_LOGIN' as const;
export const LINK_LOGIN_SUCCESS = 'LogInReducer/LINK_LOGIN_SUCCESS' as const;
//로그아웃
export const LINK_LOGOUT = 'LogInReducer/LINK_LOGOUT' as const;
export const LINK_LOGOUT_SUCCESS = 'LogInReducer/LINK_LOGOUT_SUCCESS' as const;
//회원가입
export const LINK_SIGNUP = 'LogInReducer/LINK_SIGNUP' as const;
//회원정보수정
export const UPDATE_USER = 'LogInReducer/UPDATE_USER' as const;
export const UPDATE_USER_SUCCESS = 'LogInReducer/UPDATE_USER_SUCCESS' as const;
//회원삭제
export const DELETE_USER = 'LogInReducer/DELETE_USER' as const;

/*액션함수*/
export const linkLogIn = (data: Interface.IlogInData) => ({
    type: LINK_LOGIN,
    payload: { data },
});
export const linkLogInSuccess = (data: Interface.IUsersData, token: string) => ({
    type: LINK_LOGIN_SUCCESS,
    payload: { data, token },
});
export const linkLogOut = () => ({ type: LINK_LOGOUT });
export const linkLogOutSuccess = () => ({ type: LINK_LOGOUT_SUCCESS });
export const linkSignUp = (data: Interface.IUsersData) => ({
    type: LINK_SIGNUP,
    payload: { data },
});
export const updateUser = (data: Interface.IUsersData) => ({
    type: UPDATE_USER,
    payload: { data },
});
export const updateUserSuccess = (data: Interface.IUsersData) => ({
    type: UPDATE_USER_SUCCESS,
    payload: { data },
});
export const deleteUser = (data: Interface.IUsersData) => ({
    type: DELETE_USER,
    payload: { data },
});

/*기본상태 및 리듀서*/
export interface ILogInState {
    logInState: boolean;
    logInStateData: Interface.IUsersData | null;
    bearerToken: string;
}

const initialState = {
    logInState: false,
    logInStateData: null,
    bearerToken: '',
};

type LogInAction =
    | ReturnType<typeof linkLogIn>
    | ReturnType<typeof linkLogInSuccess>
    | ReturnType<typeof linkLogOut>
    | ReturnType<typeof linkLogOutSuccess>
    | ReturnType<typeof linkSignUp>
    | ReturnType<typeof updateUserSuccess>;

export function LogInReducer(state: ILogInState = initialState, action: LogInAction) {
    switch (action.type) {
        case LINK_LOGIN_SUCCESS:
            return {
                ...state,
                logInState: true,
                logInStateData: action.payload.data,
                bearerToken: action.payload.token,
            };

        case LINK_LOGOUT_SUCCESS:
            return {
                ...state,
                logInState: false,
                logInStateData: null,
                bearerToken: '',
            };

        case UPDATE_USER_SUCCESS:
            return {
                ...state,
                logInStateData: action.payload.data,
            };

        default:
            return state;
    }
}
