import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from 'store/reducer/RootReducer';

import { showLoad, hideLoad } from 'store/reducer/LoadReducer';

export const selectLoadState = (state: State) => state.LoadReducer.loadState;

function UseLoad() {
    const dispatch = useDispatch();

    return {
        loadState: useSelector(selectLoadState),
        showLoad: useCallback(bindActionCreators(showLoad, dispatch), [dispatch]),
        hideLoad: useCallback(bindActionCreators(hideLoad, dispatch), [dispatch]),
    };
}

export default UseLoad;
