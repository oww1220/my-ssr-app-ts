import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from 'store/reducer/RootReducer';

import { historyChange } from 'store/reducer/HistoryReducer';

export const selectHistory = (state: State) => state.HistoryReducer.history;

function UseLoad() {
    const dispatch = useDispatch();

    return {
        history: useSelector(selectHistory),
        historyChange: useCallback(bindActionCreators(historyChange, dispatch), [dispatch]),
    };
}

export default UseLoad;
