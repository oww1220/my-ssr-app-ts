import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from 'store/reducer/RootReducer';

import { fetchTestList, fetchAuthList, authListReset } from 'store/reducer/GetTestListReducer';

export const selectTestLists = (state: State) => state.GetTestListReducer.testLists;
export const selectAuthObj = (state: State) => state.GetTestListReducer.authObj;

function UseLoad() {
    const dispatch = useDispatch();

    return {
        testLists: useSelector(selectTestLists),
        authObj: useSelector(selectAuthObj),
        fetchTestList: useCallback(bindActionCreators(fetchTestList, dispatch), [dispatch]),
        fetchAuthList: useCallback(bindActionCreators(fetchAuthList, dispatch), [dispatch]),
        authListReset: useCallback(bindActionCreators(authListReset, dispatch), [dispatch]),
    };
}

export default UseLoad;
