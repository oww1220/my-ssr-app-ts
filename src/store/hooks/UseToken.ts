import React, { useState, useEffect, useCallback } from 'react';
import jwt_decode from 'jwt-decode';
import * as Interface from 'interfaces/Interfaces';
import useLogIn from 'store/hooks/UseLogIn';

function UseToken() {
    const { logInState, linkLogInSuccess, linkLogOutSuccess } = useLogIn();
    const [token, setToken] = useState<string | undefined>(undefined);

    useEffect(()=>{
        console.log('token change:', token);
        /*새션토큰 변화만 있을시 로그인 링크 액션 호출해서 로직실행*/
		if(!logInState && token) {
			const decoded = jwt_decode<any>(token);
			const payload: Interface.IUsersData = decoded.payload;
			console.log(payload);
			linkLogInSuccess(payload, token);
		}
		else if(logInState && !token) {
			linkLogOutSuccess();
		}
    },[token]);

    return {
        token, 
        setToken
    };
}

export default UseToken;
