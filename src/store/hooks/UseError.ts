import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from 'store/reducer/RootReducer';

import { failureLoad } from 'store/reducer/ErrorReducer';

export const selectErrorData = (state: State) => state.ErrorReducer.errorData;

function UseLoad() {
    const dispatch = useDispatch();

    return {
        errorData: useSelector(selectErrorData),
        failureLoad: useCallback(bindActionCreators(failureLoad, dispatch), [dispatch]),
    };
}

export default UseLoad;
