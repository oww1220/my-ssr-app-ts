import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from 'store/reducer/RootReducer';

import {
    linkLogIn,
    linkLogInSuccess,
    linkLogOut,
    linkLogOutSuccess,
    linkSignUp,
    updateUser,
    deleteUser,
} from 'store/reducer/LogInReducer';

export const selectLoginState = (state: State) => state.LogInReducer.logInState;
export const selectLogInStateData = (state: State) => state.LogInReducer.logInStateData;
export const selectBearerToken = (state: State) => state.LogInReducer.bearerToken;

function UseLogIn() {
    const dispatch = useDispatch();

    return {
        logInState: useSelector(selectLoginState),
        logInStateData: useSelector(selectLogInStateData),
        bearerToken: useSelector(selectBearerToken),
        linkLogIn: useCallback(bindActionCreators(linkLogIn, dispatch), [dispatch]),
        linkLogInSuccess: useCallback(bindActionCreators(linkLogInSuccess, dispatch), [dispatch]),
        linkLogOut: useCallback(bindActionCreators(linkLogOut, dispatch), [dispatch]),
        linkLogOutSuccess: useCallback(bindActionCreators(linkLogOutSuccess, dispatch), [dispatch]),
        linkSignUp: useCallback(bindActionCreators(linkSignUp, dispatch), [dispatch]),
        updateUser: useCallback(bindActionCreators(updateUser, dispatch), [dispatch]),
        deleteUser: useCallback(bindActionCreators(deleteUser, dispatch), [dispatch]),
    };
}

export default UseLogIn;
