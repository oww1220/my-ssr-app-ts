import { all, fork } from 'redux-saga/effects';

import { watchFetchTestList, watchFetchAuthList } from 'store/saga/ListSaga';
import { watchLinkSignUp } from 'store/saga/SignUpSaga';
import { watchLinkLogIn, watchLinkLogOut } from 'store/saga/LogInSaga';
import { watchUpdateUser, watchDeleteUser } from 'store/saga/UpdateSaga';

export default function* rootSaga() {
    yield all([
        fork(watchFetchTestList),
        fork(watchLinkSignUp),
        fork(watchLinkLogIn),
        fork(watchLinkLogOut),
        fork(watchFetchAuthList),
        fork(watchUpdateUser),
        fork(watchDeleteUser),
        //fork(watchPutWrite)
    ]);
}
