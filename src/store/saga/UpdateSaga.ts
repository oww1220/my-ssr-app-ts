import { takeEvery, put, call, select, delay } from 'redux-saga/effects';
import * as ActionLogIn from 'store/reducer/LogInReducer';
import * as ActionLoad from 'store/reducer/LoadReducer';
import * as ActionError from 'store/reducer/ErrorReducer';
import * as ActionTestList from 'store/reducer/GetTestListReducer';
import * as Api from 'lib/Api';
import * as Commons from 'lib/Commons';
import Config from 'lib/Config';
import { selectHistory } from 'store/hooks/UseHistory';

function* UpdateUserSaga(action: ReturnType<typeof ActionLogIn.updateUser>): Generator<any, void, any> {
    console.log('saga!:', action);

    const history = yield select(selectHistory);

    const { data } = action.payload;

    yield put(ActionLoad.showLoad());

    try {
        const { data: qraphQlData } = yield call(Api.updateUser, data);
        //console.log( qraphQlData.data.updateUser);

        /*업데이트 정보 리덕스 스토어에 저장*/
        yield put(ActionLogIn.updateUserSuccess(qraphQlData.data.updateUser));

        /*token 정보 새로 업데이트*/
        const { data: token } = yield call(Api.resetToken, data);
        //console.log(token.data.resetToken);

        /*jwt: 쿠키 새로 업데이트*/
        Commons.removeCookies(Config.BEARERTOKEN);
        const date = new Date();
        date.setTime(date.getTime() + 1 * 60 * 1000); // 1분
        Commons.setCookies(Config.BEARERTOKEN, token.data.resetToken, { expires: date });

        /*완료후 메인페이지로!*/
        history.push('/');
    } catch (error) {
        yield put(ActionError.failureLoad(error.response));
    }
    yield put(ActionLoad.hideLoad());
}

function* DeleteUserSaga(action: ReturnType<typeof ActionLogIn.deleteUser>): Generator<any, void, any> {
    console.log('saga!:', action);

    const { data } = action.payload;

    yield put(ActionLoad.showLoad());

    try {
        //회원삭제!
        const { data: qraphQlData } = yield call(Api.deleteUser, data);
        console.log('user_id!!!!!!!!!', qraphQlData);
        //로그아웃 로직 적용
        yield alert(`${qraphQlData.data.deleteUser.user_id} 회원 탈퇴!`);
        yield put(ActionLogIn.linkLogOut());
    } catch (error) {
        yield put(ActionError.failureLoad(error.response));
    }
    yield put(ActionLoad.hideLoad());
}

export function* watchUpdateUser() {
    yield takeEvery(ActionLogIn.UPDATE_USER, UpdateUserSaga);
}

export function* watchDeleteUser() {
    yield takeEvery(ActionLogIn.DELETE_USER, DeleteUserSaga);
}
