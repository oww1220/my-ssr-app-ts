import { takeEvery, put, call, delay, select } from 'redux-saga/effects';
import * as ActionTestList from 'store/reducer/GetTestListReducer';
import * as ActionLoad from 'store/reducer/LoadReducer';
import * as ActionError from 'store/reducer/ErrorReducer';
import * as Api from 'lib/Api';
import { selectLogInStateData, selectBearerToken } from 'store/hooks/UseLogIn';

function* FetchTestListSaga(action: ReturnType<typeof ActionTestList.fetchTestList>): Generator<any, void, any> {
    console.log('saga!:', action);

    yield put(ActionLoad.showLoad());

    //yield delay(1000);
    try {
        const { data: qraphQlData } = yield call(Api.getTestList);
        console.log('allUser!', qraphQlData.data.allUser);
        yield put(ActionTestList.fetchTestSuccess(qraphQlData.data.allUser));
    } catch (error) {
        console.log('error', error);
        yield put(ActionError.failureLoad(error.response));
    }
    yield put(ActionLoad.hideLoad());
}

function* FetchAuthListSaga(action: ReturnType<typeof ActionTestList.fetchAuthList>): Generator<any, void, any> {
    console.log('saga!:', action);

    const logInStateData = yield select(selectLogInStateData);

    console.log('logInStateData', logInStateData);

    /*리덕스에 저장된 토큰을 api호출 함수에 넘겨줌*/
    const bearerToken = yield select(selectBearerToken);
    console.log('bearerToken!!!', bearerToken);

    yield put(ActionLoad.showLoad());
    try {
        const { data: qraphQlData } = yield call(Api.getAuthList, logInStateData.user_id, bearerToken);
        console.log('authUser!', qraphQlData.data.authUser);
        yield put(ActionTestList.fetchAuthSuccess(qraphQlData.data.authUser));
    } catch (error) {
        console.log('error', error);
        yield put(ActionError.failureLoad(error.response));
    }
    yield put(ActionLoad.hideLoad());
}

export function* watchFetchTestList() {
    yield takeEvery(ActionTestList.FETCH_TESTLIST, FetchTestListSaga);
}

export function* watchFetchAuthList() {
    yield takeEvery(ActionTestList.FETCH_AUTHLIST, FetchAuthListSaga);
}
