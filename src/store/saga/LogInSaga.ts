import { takeEvery, put, call, select, delay } from 'redux-saga/effects';
import * as ActionLogIn from 'store/reducer/LogInReducer';
import * as ActionLoad from 'store/reducer/LoadReducer';
import * as ActionError from 'store/reducer/ErrorReducer';
import * as ActionTestList from 'store/reducer/GetTestListReducer';
import * as Api from 'lib/Api';
import * as Commons from 'lib/Commons';
import Config from 'lib/Config';
import { selectHistory } from 'store/hooks/UseHistory';

function* LinkLogInSaga(action: ReturnType<typeof ActionLogIn.linkLogIn>): Generator<any, void, any> {
    console.log('saga!:', action);

    const history = yield select(selectHistory);

    const { data } = action.payload;

    yield put(ActionLoad.showLoad());

    //yield delay(500);
    try {
        const { status, data: qraphQlData } = yield call(Api.getUser, data);
        console.log(status, qraphQlData);

        /*로그인 정보 및 jwt: 리덕스 스토어에 저장*/
        yield put(ActionLogIn.linkLogInSuccess(qraphQlData.data.getUser.foundUser, qraphQlData.data.getUser.token));

        /*jwt: 쿠키에 저장*/
        const date = new Date();
        date.setTime(date.getTime() + 1 * 60 * 1000); // 1분
        Commons.setCookies(Config.BEARERTOKEN, qraphQlData.data.getUser.token, { expires: date });
        history.push('/');
    } catch (error) {
        yield put(ActionError.failureLoad(error.response));
    }
    yield put(ActionLoad.hideLoad());
}

function* LinkLogOutSaga(action: ReturnType<typeof ActionLogIn.linkLogOut>): Generator<any, void, any> {
    console.log('saga!:', action);

    const history = yield select(selectHistory);

    yield put(ActionLoad.showLoad());
    yield put(ActionLogIn.linkLogOutSuccess());

    /*각종 로그인시 의존했던 데이터들 reset액션으로 삭세시킴*/
    //.........
    yield put(ActionTestList.authListReset());

    /*jwt 쿠키에서 삭제*/
    Commons.removeCookies(Config.BEARERTOKEN);
    history.push('/');
    yield put(ActionLoad.hideLoad());
}

export function* watchLinkLogIn() {
    yield takeEvery(ActionLogIn.LINK_LOGIN, LinkLogInSaga);
}

export function* watchLinkLogOut() {
    yield takeEvery(ActionLogIn.LINK_LOGOUT, LinkLogOutSaga);
}
