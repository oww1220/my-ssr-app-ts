import { takeEvery, put, call, select, delay } from 'redux-saga/effects';
import * as ActionLogIn from 'store/reducer/LogInReducer';
import * as ActionLoad from 'store/reducer/LoadReducer';
import * as ActionError from 'store/reducer/ErrorReducer';
import * as Api from 'lib/Api';
import { selectHistory } from 'store/hooks/UseHistory';

function* LinkSignUpSaga(action: ReturnType<typeof ActionLogIn.linkSignUp>): Generator<any, void, any> {
    console.log('saga!:', action);

    const history = yield select(selectHistory);

    const { data } = action.payload;

    yield put(ActionLoad.showLoad());

    yield delay(500);
    try {
        const { status, data: qraphQlData } = yield call(Api.createUser, data);
        console.log(status, qraphQlData);
        if (qraphQlData.errors) {
            yield alert(qraphQlData.errors[0].message);
            yield put(ActionLoad.hideLoad());
            return;
        }
        if (status === 200) history.push('/LogIn');
    } catch (error) {
        yield put(ActionError.failureLoad(error.response));
    }
    yield put(ActionLoad.hideLoad());
}

export function* watchLinkSignUp() {
    yield takeEvery(ActionLogIn.LINK_SIGNUP, LinkSignUpSaga);
}
