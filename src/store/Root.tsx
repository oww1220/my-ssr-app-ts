import React from 'react';
import { Store } from 'redux';
import { Provider } from 'react-redux';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-boost';
import App from 'App';
import RootProvider from 'store/RootProvider';

interface RootProps {
	store:Store;
	client:ApolloClient<unknown>;
	cookie?: any;
}
const Root = ({store, client, cookie}: RootProps)=>{
	return (
		<Provider store={store}>
			<ApolloProvider client={client}>
				<RootProvider cookie={cookie}>
					<App />
				</RootProvider>
			</ApolloProvider>
		</Provider>
	);
};

export default Root;