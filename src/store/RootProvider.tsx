import React from 'react';
import jwt_decode from 'jwt-decode';
import * as Interface from 'interfaces/Interfaces';
import useLogIn from 'store/hooks/UseLogIn';

interface IRootProvider {
	children: JSX.Element;
	cookie?: any;
}

const RootProvider = ({children, cookie}: IRootProvider)=>{
	const { linkLogInSuccess } = useLogIn();

	//ssr용 로그인 체크 로직: 브라우저에서 받은 쿠키를 이용: cookie 파라미터 안에는 express 웹서버에서 받은 req.cookies 객체정보가 들어감
	if(cookie && cookie.BearerToken !== undefined) {
		//console.log('cookie.BearerToken!!!!', cookie.BearerToken);
		//jwt 디코드시켜서 로그인상태및 기본정보 업데이트!
		const decoded = jwt_decode<any>(cookie.BearerToken);
		const payload: Interface.IUsersData = decoded.payload;
		linkLogInSuccess(payload, cookie.BearerToken);
	}

	return (
		children
	);
}

export default RootProvider;