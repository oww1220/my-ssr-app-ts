import { createStore, applyMiddleware, Store } from 'redux';
import createSagaMiddleware,  { END, SagaMiddleware } from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer, { State } from 'store/reducer/RootReducer';

declare module 'redux' {
	interface Store {
		close: ()=>END;
		runSaga: SagaMiddleware['run'];
	}
}

const ConfigureStore = (preloadedState?: State | undefined)=> {

	const sagaMiddleware = createSagaMiddleware();
	
	const store:Store = createStore(
		rootReducer,
		preloadedState,
		composeWithDevTools(applyMiddleware(sagaMiddleware)),
	);
	store.runSaga = sagaMiddleware.run;
	store.close = () => store.dispatch(END);

	return store;
};


export default ConfigureStore;