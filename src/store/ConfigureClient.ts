import ApolloClient from 'apollo-boost';
import { InMemoryCache } from 'apollo-cache-inmemory';
import fetch from 'node-fetch';  

const ConfigureClient = ()=> {

  const baseUrl = process.env.REACT_APP_APOLLO_URL;
  const cache = new InMemoryCache();
  const client = new ApolloClient({
      //ssrMode: true,
      uri: `${baseUrl}/graphql`,
      cache,
      fetch: fetch as any,
  });

  return client;
};


export default ConfigureClient;