import React from 'react';
import ReduxActions from 'redux';
import configureStore from 'redux-mock-store';
import * as Const from '__test__/Mock/Const';
import * as ActionTestList from 'store/reducer/GetTestListReducer';

const middlewares: any[] = [];
const mockStore = configureStore(middlewares);

// redux-mock-store는 상태를 업데이트 하지 않고 디스패치에 전달된 작업만 기록하기 때문에, store.dispatch() 구문 실행 후 store.getState()로 변경된 상태를 확인하는 것이 불가능합니다.
// 해당 라이브러리가 리듀서 관련 로직이 아닌 액션 관련 로직을 테스트하도록 설계되었기 때문에 이는 어쩔 수 없는 부분이 아닌가 생각합니다.

describe('GetTestListReducer: action creator 함수 테스트', () => {
    it('fetchTestList 액션 생성 함수는 액션을 의도한대로 생성해야 한다.', () => {
        const store = mockStore(ActionTestList.initialState); //초기화 상태 넣어줌
        store.dispatch(ActionTestList.fetchTestList()); //액션 dispatch!!

        //액션 함수 return value 비교!!
        expect(store.getActions()).toEqual([
            //store.dispatch() 구문을 실행할 때의 action은 인스턴스 내부 배열에 저장되어 실행되기 때문에 getActions() 역시 배열을 반환합니다.
            {
                type: ActionTestList.FETCH_TESTLIST,
            },
        ]);

        //다음 테스트 엑션을위해 클리어함!!
        store.clearActions();

        //파라미터가 있는 액션!
        store.dispatch(ActionTestList.fetchTestSuccess(Const.Fake_allUser.data.data.allUser)); //액션 dispatch!!
        expect(store.getActions()).toEqual([
            //store.dispatch() 구문을 실행할 때의 action은 인스턴스 내부 배열에 저장되어 실행되기 때문에 getActions() 역시 배열을 반환합니다.
            {
                type: ActionTestList.TESTLIST_SUCCESS,
                payload: { testLists: Const.Fake_allUser.data.data.allUser },
            },
        ]);
    });
});

describe('GetTestListReducer: Reducer 테스트', () => {
    it('action에 빈 객체를 넘겨 줄 경우 initialState를 반환해야 한다.', () => {
        expect(
            //param 1: 기본state, param2: 액션 생성함수 리턴값 (액션이 없는경우는 빈객체)
            ActionTestList.GetTestListReducer(ActionTestList.initialState, {} as ReduxActions.Action<any>), //리듀서에 빈액션을 넘기면 기본상태를 반납해야된다!
        ).toEqual(ActionTestList.initialState);
    });

    it('fetchTestSuccess action 반환테스트!', () => {
        expect(
            //param 1: 기본state, param2: 액션 생성함수 리턴값
            ActionTestList.GetTestListReducer(
                ActionTestList.initialState,
                ActionTestList.fetchTestSuccess(Const.Fake_allUser.data.data.allUser),
            ), //리듀서에 fetchTestSuccess액션을 넘김!
        ).toEqual({
            testLists: Const.Fake_allUser.data.data.allUser,
            authObj: null,
        });
    });
});
