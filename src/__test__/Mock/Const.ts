//axios 에서 한번, graphql에서 한번 이렇게 2번 구조체로 랩핑이 되어 depth가 깊어짐;;;;
export const Fake_allUser = {
    data: {
        data: {
            //<---api로 받는 실제 데이터
            allUser: [
                {
                    _id: '111',
                    user_id: 'aa',
                    user_name: 'aa',
                    age: 11,
                    gender: '남',
                    password: '1234',
                },
            ],
            //api로 받는 실제 데이터--->
        },
    },
};

export const Fake_authUser = {
    data: {
        data: {
            //<---실제 데이터
            authUser: {
                couponCnt: 1,
                estimateSaveCnt: 1,
            },
            //실제 데이터--->
        },
    },
};

export const Fake_updateUser = {
    data: {
        data: {
            //<---실제 데이터
            updateUser: {
                _id: '111',
                user_id: 'aa',
                user_name: 'aa',
                age: 11,
                gender: '남',
            },
            //실제 데이터--->
        },
    },
};

export const Fake_deleteUser = {
    data: {
        data: {
            //<---실제 데이터
            deleteUser: {
                user_id: 'aa',
            },
            //실제 데이터--->
        },
    },
};

export const Fake_logInStateData = {
    _id: '111',
    user_id: 'aa',
    user_name: 'aa',
    age: 11,
    gender: '남',
    password: '1234',
};

export const Fake_bearerToken = 'bearerToken';
