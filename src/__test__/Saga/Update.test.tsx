import React from 'react';
import { call, put, select } from 'redux-saga/effects';
import * as matchers from 'redux-saga-test-plan/matchers';
import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';
import { createMemoryHistory } from 'history';
import Config from 'lib/Config';
import * as Commons from 'lib/Commons';
import * as Const from '__test__/Mock/Const';
import * as Api from 'lib/Api';
import * as ActionLogIn from 'store/reducer/LogInReducer';
import * as ActionLoad from 'store/reducer/LoadReducer';
import * as ActionError from 'store/reducer/ErrorReducer';
import { watchUpdateUser, watchDeleteUser } from 'store/saga/UpdateSaga';
import { selectHistory } from 'store/hooks/UseHistory';

describe('watchUpdateUser', () => {
    it('saga!:UpdateUser success!!', () => {
        const date = new Date();
        date.setTime(date.getTime() + 1 * 60 * 1000);
        return expectSaga(watchUpdateUser)
            .dispatch(ActionLogIn.updateUser(Const.Fake_logInStateData))
            .provide([
                [select(selectHistory), createMemoryHistory()],
                [call(Api.updateUser, Const.Fake_logInStateData), Const.Fake_updateUser], //DB update!
                [call(Api.resetToken, Const.Fake_logInStateData), Const.Fake_bearerToken], //reset bearertoken!
                [call(Commons.removeCookies, Config.BEARERTOKEN), {}], //쿠키삭제
                [call(Commons.setCookies, Config.BEARERTOKEN, Const.Fake_bearerToken, { expires: date }), {}], //쿠키설정
            ])
            .put(ActionLoad.showLoad())
            .put(ActionLogIn.updateUserSuccess(Const.Fake_updateUser.data.data.updateUser)) //redux store update!
            .put(ActionLoad.hideLoad())
            .silentRun();
    });

    it('saga!:UpdateUser failure!!', () => {
        const error = throwError(new Error('UpdateUser error!!'));
        return expectSaga(watchUpdateUser)
            .dispatch(ActionLogIn.updateUser(Const.Fake_logInStateData))
            .provide([
                [select(selectHistory), createMemoryHistory()],
                [call(Api.updateUser, Const.Fake_logInStateData), error],
            ])
            .put(ActionLoad.showLoad())
            .put(ActionError.failureLoad(error.response))
            .put(ActionLoad.hideLoad())
            .silentRun();
    });
});

describe('watchDeleteUser', () => {
    it('saga!:DeleteUser success!!', () => {
        return expectSaga(watchDeleteUser)
            .dispatch(ActionLogIn.deleteUser(Const.Fake_logInStateData))
            .provide([
                [call(Api.deleteUser, Const.Fake_logInStateData), Const.Fake_deleteUser], //DB delete!
            ])
            .put(ActionLoad.showLoad())
            .put(ActionLogIn.linkLogOut()) //DB delete and logout!
            .put(ActionLoad.hideLoad())
            .silentRun();
    });

    it('saga!:DeleteUser failure!!', () => {
        const error = throwError(new Error('DeleteUser error!!'));
        return expectSaga(watchDeleteUser)
            .dispatch(ActionLogIn.deleteUser(Const.Fake_logInStateData))
            .provide([[call(Api.deleteUser, Const.Fake_logInStateData), error]])
            .put(ActionLoad.showLoad())
            .put(ActionError.failureLoad(error.response))
            .put(ActionLoad.hideLoad())
            .silentRun();
    });
});
