import React from 'react';
import { call, put, select } from 'redux-saga/effects';
import * as matchers from 'redux-saga-test-plan/matchers';
import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';

import * as Const from '__test__/Mock/Const';
import * as Api from 'lib/Api';
import * as ActionTestList from 'store/reducer/GetTestListReducer';
import * as ActionLoad from 'store/reducer/LoadReducer';
import * as ActionError from 'store/reducer/ErrorReducer';
import { watchFetchTestList, watchFetchAuthList } from 'store/saga/ListSaga';
import { selectLogInStateData, selectBearerToken } from 'store/hooks/UseLogIn';

describe('watchFetchTestList', () => {
    it('saga!:FetchTestList success!!', () => {
        //redux-saga를 사용하면서 작성하는 generator 함수는 내부에서 yield를 사용한다. 그리고 이는 effect라고 부르는 객체를 반환하는데 이 객체를 middleware에서 받아 실행하고 그 결과를 다시 반환한다. 즉 어떤 작업을 생성하는 곳과 실제로 실행하는 곳이 분리되어 있는 것이다. 실행은 middleware에서 하기 때문에 우리는 직접 작성한 코드(생성을 담당하는)를 테스트해야 하는데, 두 부분이 분리되어 있는 덕분에 중간에 끼어들어 실행하지 않고도 테스트를 할 수 있게 되는 것이다.

        return expectSaga(watchFetchTestList) //제너레이터 함수(실제 로직이 들어있는)를 watch하고 있는 함수를 test로 업로드
            .withReducer(ActionTestList.GetTestListReducer) //redux state를 redux-saga와 함께 테스트 하기위해 withReducer 메서드를 사용해서 리듀서에 연결해줍니다.
            .dispatch(ActionTestList.fetchTestList()) //구독하고 있는 액션이 dispatch되면 redux-saga는 FetchTestListSaga를 포크하고 액션을 넘긴다.

            .provide([[call(Api.getTestList), Const.Fake_allUser]]) //provide 메서드(내장된 목킹)는 매처(matcher)-값(value) 쌍을 배열로 받는다. 각 매처-값 쌍은 매칭할 이펙트와 이에 반환할 가짜 값을 엘리먼트로 가진 배열이다. redux-saga-test-plan은 이펙트를 가로채고, 매칭을 확인한 후 redux-saga에 이펙트 처리를 넘기지 않고 바로 가짜 값을 반환하도록 한다.
            .put(ActionLoad.showLoad()) //put 메서드는 액션이 발생했는지 확인해줍니다.
            .put(ActionTestList.fetchTestSuccess(Const.Fake_allUser.data.data.allUser))
            .hasFinalState({
                testLists: Const.Fake_allUser.data.data.allUser,
                authObj: null,
            }) //hasFinalState를 사용해 redux store의 최종적인 상태를 확인할 수 있습니다.
            .put(ActionLoad.hideLoad())
            .silentRun(); //사가를 실행시키는 방법에는 run과 silentRun 2가지의 메서드가 있습니다.
        //일반적으로 사가 테스트에서는 run을 사용해서 경고메시지를 확인하는 것이 맞지만 takeLatest를 사용하는 경우에는 silentRun를 사용합니다.
        //그 이유는..!👆
        //takeLatest는 무한 루프를 돌기때문에 redux-saga-test-plan에서 경고 메시지와 함께 사가를 타임아웃 시키는데, 이때 slientRun를 사용하면 경고 메시지를 생략할 수 있습니다.
    });

    it('saga!:FetchTestList failure!!', () => {
        const error = throwError(new Error('FetchTestList error!!'));

        return expectSaga(watchFetchTestList)
            .dispatch(ActionTestList.fetchTestList())
            .provide([[call(Api.getTestList), error]])
            .put(ActionLoad.showLoad())
            .put(ActionError.failureLoad(error.response))
            .put(ActionLoad.hideLoad())
            .silentRun();
    });
});

describe('watchFetchAuthList', () => {
    it('saga!:FetchAuthList success!!', () => {
        return expectSaga(watchFetchAuthList)
            .dispatch(ActionTestList.fetchAuthList())
            .provide([
                [select(selectLogInStateData), Const.Fake_logInStateData], //saga에서 select로 불러오는 state는 mockable해야됨!!
                [select(selectBearerToken), Const.Fake_bearerToken], //saga에서 select로 불러오는 state는 mockable해야됨!!
                [call(Api.getAuthList, Const.Fake_logInStateData.user_id, Const.Fake_bearerToken), Const.Fake_authUser],
            ])
            .put(ActionLoad.showLoad())
            .put(ActionTestList.fetchAuthSuccess(Const.Fake_authUser.data.data.authUser))

            .put(ActionLoad.hideLoad())
            .silentRun();
    });

    it('saga!:FetchAuthList failure!!', () => {
        const error = throwError(new Error('FetchAuthList error!!'));
        return expectSaga(watchFetchAuthList)
            .dispatch(ActionTestList.fetchAuthList())
            .provide([
                [select(selectLogInStateData), Const.Fake_logInStateData],
                [select(selectBearerToken), Const.Fake_bearerToken],
                [call(Api.getAuthList, Const.Fake_logInStateData.user_id, Const.Fake_bearerToken), error],
            ])
            .put(ActionLoad.showLoad())
            .put(ActionError.failureLoad(error.response))
            .put(ActionLoad.hideLoad())
            .silentRun();
    });
});

/*--------------비동기 코드 test 예시--------------
//callback을 사용하는 코드 예시
const fetchDataCallback = (cb:any) => {
	setTimeout(() => {
	console.log("wait 3.5 sec.");
		cb('peanut butter');
	}, 3500);
};
test('the data is peanut butter', done => {
	function callback(data: any) {
		try {
			expect(data).toBe('peanut butter');
			done();
		} catch (error) {
			done(error);
		}
	}

	fetchDataCallback(callback);
});


const fetchDataPromise = () => {
	return new Promise(resolve => {
		return setTimeout(() => {
		console.log("wait 3.5 sec.");
		resolve('peanut butter');
		}, 3500);
	});
};

const fetchDataThrowError = () => {
	return new Promise((resolve, reject) => {
		return setTimeout(() => {
			console.log("wait 3.5 sec.");
			reject('error reason');
		}, 3500);
	});
};

describe('Promises resolves/rejects', () => {
	test('the data is peanut butter', () => {
		return expect(fetchDataPromise()).resolves.toBe('peanut butter');
	});

	test('the test is rejected', () => {
		expect.assertions(1);
		return expect(fetchDataThrowError()).rejects.toMatch('error reason');
	})
});

describe('Async/Await with resolves/rejects', () => {
	test('the data is peanut butter', async () => {
		await expect(fetchDataPromise()).resolves.toBe('peanut butter');
	});

	test('the fetch fails with an error', async () => {
		expect.assertions(1);
		await expect(fetchDataThrowError()).rejects.toMatch('error reason');
	});
});

/*--------------비동기 코드 test 예시--------------end*/
