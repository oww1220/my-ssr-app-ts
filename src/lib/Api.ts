import axios from 'axios';
import { gql } from 'apollo-boost';
import { print, Token } from 'graphql';
import * as Interface from 'interfaces/Interfaces';
import Config from 'lib/Config';
import * as Commons from 'lib/Commons';

const baseUrl = Config.BASEURL;
const BaseURL = `${baseUrl}/graphql`;

export const GET_ALLUSER = gql`
    query GET_ALLUSER {
        allUser {
            _id
            user_name
            age
            gender
        }
    }
`;
export const GET_AUTHUSER = gql`
    query GET_AUTHUSER($user_id: String!) {
        authUser(user_id: $user_id) {
            couponCnt
            estimateSaveCnt
        }
    }
`;

export const GET_USER = gql`
    query GET_USER($user_id: String!, $password: String!) {
        getUser(user_id: $user_id, password: $password) {
            foundUser {
                _id
                user_id
                user_name
                age
                gender
            }
            token
        }
    }
`;
export const RESET_TOKEN = gql`
    query RESET_TOKEN($user_id: String!) {
        resetToken(user_id: $user_id)
    }
`;

export const CREATE_USER = gql`
    mutation CREATE_USER($user_id: String!, $user_name: String!, $password: String!, $gender: String!, $age: Int!) {
        createUser(
            input: { user_id: $user_id, user_name: $user_name, password: $password, gender: $gender, age: $age }
        ) {
            user_id
            user_name
            gender
            age
        }
    }
`;

export const UPDATE_USER = gql`
    mutation UPDATE_USER($_id: ID!, $user_id: String, $user_name: String, $gender: String, $age: Int) {
        updateUser(_id: $_id, input: { user_id: $user_id, user_name: $user_name, gender: $gender, age: $age }) {
            _id
            user_id
            user_name
            gender
            age
        }
    }
`;

export const DELETE_USER = gql`
    mutation DELETE_USER($_id: ID!) {
        deleteUser(_id: $_id) {
            user_id
        }
    }
`;

export const getTestList = () => {
    return axios({
        method: 'post',
        url: BaseURL,
        data: {
            query: print(GET_ALLUSER),
        },
    });
};

export const getAuthList = (user_id: string, bearerToken: string) => {
    const Token = bearerToken || Commons.getBearerToken();
    return axios({
        method: 'post',
        url: BaseURL,
        data: {
            query: print(GET_AUTHUSER),
            variables: {
                user_id: String(user_id),
            },
        },
        headers: {
            Authorization: 'Bearer ' + Token,
        },
    });
};

export const getUser = (data: Interface.IlogInData) => {
    //const Token  = Commons.getBearerToken();
    return axios({
        method: 'post',
        url: BaseURL,
        data: {
            query: print(GET_USER),
            variables: {
                user_id: String(data.user_id),
                password: String(data.password),
            },
        },
        /*
        headers: {
            Authorization: 'Bearer ' + Token,
        }
        */
    });
};

export const resetToken = (data: Interface.IUsersData) => {
    const Token = Commons.getBearerToken();
    return axios({
        method: 'post',
        url: BaseURL,
        data: {
            query: print(RESET_TOKEN),
            variables: {
                user_id: String(data.user_id),
            },
        },
        headers: {
            Authorization: 'Bearer ' + Token,
        },
    });
};

export const createUser = (data: Interface.IUsersData) => {
    //const Token  = Commons.getBearerToken();
    return axios({
        method: 'post',
        url: BaseURL,
        data: {
            query: print(CREATE_USER),
            variables: {
                user_id: String(data.user_id),
                user_name: String(data.user_name),
                password: String(data.password),
                gender: String(data.gender),
                age: Number(data.age),
            },
        },
        /*
        headers: {
            Authorization: 'Bearer ' + Token,
        }
        */
    });
};

export const updateUser = (data: Interface.IUsersData) => {
    const Token = Commons.getBearerToken();
    return axios({
        method: 'post',
        url: BaseURL,
        data: {
            query: print(UPDATE_USER),
            variables: {
                _id: String(data._id),
                user_id: String(data.user_id),
                user_name: String(data.user_name),
                gender: String(data.gender),
                age: Number(data.age),
            },
        },
        headers: {
            Authorization: 'Bearer ' + Token,
        },
    });
};
export const deleteUser = (data: Interface.IUsersData) => {
    const Token = Commons.getBearerToken();
    return axios({
        method: 'post',
        url: BaseURL,
        data: {
            query: print(DELETE_USER),
            variables: {
                _id: String(data._id),
            },
        },
        headers: {
            Authorization: 'Bearer ' + Token,
        },
    });
};
