const Config = {
    BEARERTOKEN: 'BearerToken',
    BASEURL: process.env.REACT_APP_APOLLO_URL,
    APP_ENV: process.env.REACT_APP_ENV,
    APP_VERSION: process.env.REACT_APP_VERSION,
    APP_NAME: process.env.REACT_APP_NAME,
};

export default Config;
