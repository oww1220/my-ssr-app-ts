import Cookies, { CookieAttributes } from 'js-cookie';
import Config from 'lib/Config';

/*Cookies*/
export const getCookies = (name: string) => {
    return Cookies.get(name);
};

export const setCookies = (name: string, value: string, option?: CookieAttributes) => {
    Cookies.set(name, value, option);
};

export const removeCookies = (name: string, option?: CookieAttributes) => {
    Cookies.remove(name, option);
};

export const getBearerToken = () => {
    return getCookies(Config.BEARERTOKEN);
};

/*sessionStorage*/
export const addStorage = (name: string, value: string) => {
    sessionStorage.setItem(name, value);
};

export const getStorage = (name: string) => {
    return sessionStorage.getItem(name);
};

export const removeStorage = (name: string) => {
    sessionStorage.removeItem(name);
};

/*특수문자 삭제 함수*/
export const regExp = (str: string) => {
    const reg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
    //특수문자 검증
    if (reg.test(str)) {
        //특수문자 제거후 리턴
        return str.replace(reg, '');
    } else {
        //특수문자가 없으므로 본래 문자 리턴
        return str;
    }
};
