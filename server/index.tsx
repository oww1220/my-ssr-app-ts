import path from 'path';
import fs from 'fs';
import express from 'express';
import unless from 'express-unless';
import urls from 'url';
import ip from 'ip';
import serialize from 'serialize-javascript';
import queryString from 'query-string';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';

/*react*/
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter as Router, matchPath } from 'react-router-dom';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import RouteList, { IRouteList } from '@src/router/RouteList';
import Root from '@src/store/Root';

/*redux*/
import ConfigureStore from '@src/store/ConfigureStore';
import rootSaga from '@src/store/saga/RootSaga';

/*apollo*/
import { getDataFromTree } from 'react-apollo';
import ConfigureClient from '@src/store/ConfigureClient';  


const PORT = process.env.PORT || 3006;
const IP = ip.address();
const app = express();
const staticData:any = express.static('./build');
staticData.unless = unless;

/*gzip 압축 사용*/
app.use(compression());

/*cross origin resource share*/
//app.use(cors());

// parse JSON and url-encoded query
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// parse cookie
app.use(cookieParser());

/*static 파일 처리*/
app.use(staticData.unless((req:any) => {
    const ext = urls.parse(req.originalUrl).pathname;
    if(!ext) return;
    const extSubstr = ext.substr(-3);
    //console.log('!!!!!!!!!req.originalUrl!!!!!!', req.originalUrl, extSubstr);
    return !['css', 'svg', 'map', 'son', 'png', 'peg', 'jpg', 'gif', 'ico', '.js', 'off', 'ff2'].includes(extSubstr);
}));

/*리액트 로직 처리*/
app.use( async (req, res) => {
    console.log('react-logic!');

    /*쿠키*/
    const cookie = req.cookies;

    /*apollo*/
    const client = ConfigureClient();

    /*Router*/
    const context: {url?: string} = {};

    /*helmet*/
    const helmetContext: {helmet?: any} = {};

    /*redux, redux-saga watch start*/
    const store = ConfigureStore();
    const runStore = store.runSaga(rootSaga);

    /*컴퍼넌트*/
    const root = 
    <HelmetProvider context={helmetContext}>
        <Router location={req.url} context={context}>
            <Root store={store} client={client} cookie={cookie}/>    
        </Router>
    </HelmetProvider>;

    /*리액트 랜더링*/
    let dataApp = ReactDOMServer.renderToString(root);

    /*etc*/
    const indexFile = path.resolve('./build/index.html');
    const { helmet } = helmetContext;
    
    /*redux action call function*/
    RouteList.forEach((route:IRouteList, idx:number) => {
        const reqUrl = req.url.split('?')[0];
        const reqQueryString = req.url.split('?')[1];
        const requestInfo = matchPath(reqUrl, route);
        const params = queryString.parse(reqQueryString);
        if (requestInfo && route.component && route.component.getInitialData) {
            route.component.getInitialData(params);
        }
    });

    /*apollo graphQL callback 호출 데이터 get*/
    await getDataFromTree(root).then((content) => {
    }).catch((e) => {
        console.log(e.message)
        return res.status(500).send(e.message)
    });
    
    /*redux-saga end callback 호출 데이터 get*/
    runStore.toPromise().then(() => {

        console.log('sagas complete');
        //console.log('process.env',process.env.REACT_APP_APOLLO_URL);
        const finalState = store.getState();
        //console.log('finalState', finalState);

        /*redux-saga 이후 리액트 랜더링*/
        dataApp = ReactDOMServer.renderToString(root);

        /*redirect*/            
        //console.log('context.url', context.url);
        if(context.url){
            return res.redirect(301, context.url);                                
        }

        /*최종 리턴 파일*/
        fs.readFile(indexFile, 'utf8', (err, data) => {
            if (err) {
                console.error('Something went wrong:', err);
                return res.status(500).send('Oops, better luck next time!');
            }
    
            return res.send(
                data
                .replace('<div id="root"></div>', `<div id="root">${dataApp}</div>`)
                .replace('<script class="init"></script>', `<script class="init">window.__APP_INITIAL_STATE__ = ${serialize(finalState)};</script>`)
                .replace('<meta helmet>', `${helmet.title.toString()}${helmet.meta.toString()}${helmet.link.toString()}`)
            );
        });

    }).catch((e) => {
        console.log(e.message)
        return res.status(500).send(e.message)
    });

    /*redux-saga watch end*/
    store.close();
});

/*listen success*/
const server = app.listen(PORT, () => {
    console.log(`SSR running on ip: ${IP} port: ${PORT}`);
});

/*Keep-Alive time config*/
server.keepAliveTimeout = 60000 * 2;
